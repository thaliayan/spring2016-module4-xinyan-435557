import re, sys, os
if len(sys.argv)<2:
    sys.exit("Usage: %s filename" % sys.argv[0])
filename = sys.argv[1]
if not os.path.exists(filename):
    sys.exit("Error: File '%s' not found" % sys.argv[1])
f = open(filename)
class baseball:
    bats=0
    hits=0
    players= {}
    def __init__(self,name):
        self.name = name 
        self.bats = 0
        self.hits = 0
        baseball.players[name]=self
    def baseball_game(self,bats,hits):
        self.bats+=bats
        self.hits+=hits
    def avg_game(self):
        if self.bats>0:
             return "{0:.3f}".format(round(self.hits/float(self.bats) ,3))
        else:
             return 0
    def printBaseball(self):
        print self.name,': ',self.avg_game()
    
baseball_regex=re.compile(r"\b(^[A-Za-z]+\s[A-Za-z]+)\sbatted\s(\d{1})\stimes\swith\s(\d{1})\s\b")
def baseball_players(test):
    match = baseball_regex.match(test)
    if match is not None:
        return match.group(1)
    else:
        return False
def bats_players(test):
    match= baseball_regex.match(test)
    if match is not None:
        return match.group(1)
    else:
        return False
def bats_players(test):
    match= baseball_regex.match(test)
    if match is not None:
        return match.group(2)        
    else:
        return False
def hits_players(test):
    match= baseball_regex.match(test)
    if match is not None:
        return match.group(3)        
    else:
        return False
for line in f:
    name=baseball_players(line)
    bats=bats_players(line)
    hits=hits_players(line)
    if name is not False:
        if not baseball.players.has_key(name):
            baseball(name)
            baseball.players[name].baseball_game(int(bats),int(hits))
        else:
            baseball.players[name].baseball_game(int(bats),int(hits))
for value in sorted(baseball.players.values(),key=lambda v: v.avg_game() , reverse=True):
    value.printBaseball()
   
f.close()

